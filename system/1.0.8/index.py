# -*- coding: utf-8 -*-
import os
import time
import json
import tempfile

import chardet
import pygame
from pymediainfo import MediaInfo
import shutil
from os.path import expanduser
import win32api
import win32com.client


def playSound(params):
    try:
        path = params["path"]
        dllPath = params['uiauto_config']["client_dir"] + "\\env\\python\\win32\\MediaInfo.dll"
        if not os.path.isfile(path):
            raise Exception("不是文件")
        if not os.path.exists(path):
            raise Exception("文件不存在")
        if not os.path.exists(dllPath):
            shutil.copy(params['uiauto_config']['plugins_dir']+r"\system\MediaInfo.dll", params['uiauto_config']["client_dir"] + r"\\env\\python\\win32")

        pygame.mixer.init()
        pygame.mixer.music.load(path)

        music = MediaInfo.parse(path).to_json()
        music = json.loads(music)
        length = music['tracks'][0]['duration'] / 1000

        while True:
            pygame.mixer.music.play(loops=0, start=0.0)
            pygame.mixer.music.set_volume(0.5)
            time.sleep(length+1)
            pygame.mixer.music.stop()
            break
    except Exception as e:
        raise e


def fileIsExist(params):
    path = params['path']
    data = False
    try:
        if os.path.exists(path):
            data = True
    except Exception as e:
        raise e
    return data


def getEnv(params):
    try:
        env = params['env']
        if env == '':
            return dict(os.environ)
        else:
            return os.getenv(env)
    except KeyError:
        return "没有该环境变量"
    except Exception as e:
        raise e


def setEnv(params):
    try:
        env = params["env"]
        value = params["value"]
        if params['isForever'] == 'no':
            os.environ[env] = value
        else:
            cmd = "setx %s %s" % (env, value)
            params['cmd'] = cmd
            # execPowerShell(params)
            execCommand(cmd)
    except Exception as e:
        raise e


def execCommand(params):
    try:
        cmd = params["cmd"]
        result = os.popen(cmd, 'r')
        return result.read()
    except Exception as e:
        raise e


def execPowerShell(params):
    try:
        cmd = params["cmd"]
        PowerShellCommand = 'powershell -Command "' + str(cmd) + '"'
        output = os.popen(PowerShellCommand, 'r')
        return output.read()
    except Exception as e:
        raise e


def getSystemPath(params):
    try:
        path = params['type']
        if path == "desktop":
            return os.path.join(GetUserPath(params), "Desktop")
        elif path == "start":
            objShell = win32com.client.Dispatch("WScript.Shell")
            return objShell.SpecialFolders("StartMenu")
        elif path == "install":
            return os.environ['programfiles']
        elif path == "windows":
            return win32api.GetWindowsDirectory()
        elif path == "system":
            return win32api.GetSystemDirectory()
    except Exception as e:
        raise e


def getTempPath(params):
    try:
        return tempfile.gettempdir()
    except Exception as e:
        raise e


def GetUserPath(params):
    try:
        return expanduser("~")
    except Exception as e:
        raise e


if __name__ == "__main__":
    pass
