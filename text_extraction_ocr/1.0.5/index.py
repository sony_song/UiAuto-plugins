import json
import os
import base64
import urllib.request as urllib2
import urllib
import time
import requests


def get_token(api_key='', secret_ket=''):
    # https://ai.baidu.com/docs#/Auth/75d80ed1
    # https://console.bce.baidu.com/ai/?_=1566193744774&fromai=1#/ai/ocr/app/detail~appId=289184
    # api_key = 'R5LAXesAqaNYunPWLb5UHM3h'
    # secret_ket = 'RdLi2pn2usBfGVTSSHedQUlrkO1FUnIV'
    token_url = f'https://aip.baidubce.com/oauth/2.0/token?' \
        f'grant_type=client_credentials&client_id={api_key}&client_secret={secret_ket}&'
    res = requests.post(token_url)
    if 'access_token' in res.json().keys():
        access_token = res.json()['access_token']
        return access_token
    else:
        return 'error'


def encode_image(path):
    try:
        size = os.path.getsize(path)
        if size > 4194304:
            return "image size too big"
        f = open(path, 'rb')
        img = base64.b64encode(f.read())
        return img
    except Exception as error:
        return error


def input_photo(params):
    url = params['url']
    request_params = params['request_params']
    encode_params = urllib.parse.urlencode(request_params).encode("utf-8")
    request = urllib2.Request(url, encode_params)
    request.add_header('Content-Type', 'application/x-www-form-urlencoded')
    response = urllib2.urlopen(request)
    content = response.read()
    return str(content.decode("utf-8"))


def ocr_set_params(params):
    access_token = get_token(params['api_key'], params['secret_ket'])
    if 'error' not in access_token:
        dicts = {}
        dicts['type'] = params['type']
        dicts['path'] = params['path']
        dicts['request_params'] = {'image': encode_image(dicts['path'])}
        if dicts['type'] == '身份证':
            print('身份证')
            dicts['request_params']['id_card_side'] = 'front'
            dicts['url'] = 'https://aip.baidubce.com/rest/2.0/ocr/v1/idcard?access_token=' + access_token
            return json.dumps(input_photo(dicts), ensure_ascii=False)

        elif dicts['type'] == '增值税发票':
            print('增值税发票')
            dicts['url'] = 'https://aip.baidubce.com/rest/2.0/ocr/v1/vat_invoice?access_token=' + access_token
            data = json.loads(input_photo(dicts))
            InvoiceCode = data['words_result']['InvoiceCode']
            InvoiceNum = data['words_result']['InvoiceNum']
            TotalAmount = data['words_result']['TotalAmount']
            InvoiceDate = data['words_result']['InvoiceDate']
            CheckCode = data['words_result']['CheckCode']
            if CheckCode:
                CheckCode = data['words_result']['CheckCode'][-6:]
                # print(InvoiceCode, InvoiceNum, InvoiceDate, TotalAmount, CheckCode)
            if InvoiceCode and InvoiceNum and InvoiceDate and (TotalAmount or CheckCode):
                InvoiceDate = str(InvoiceDate).replace("年", "-").replace("月", "-").replace("日", "")
                today_y = str(int(time.strftime('%Y', time.localtime(time.time()))) - 1)
                last_date = today_y + time.strftime('-%m-%d', time.localtime(time.time()))
                if InvoiceDate <= last_date:
                    data["Authenticity"] = "Expired"
                    return json.dumps(data, ensure_ascii=False)
                url = 'https://fapiao.youshang.com/GuoShuiAction.do?action=findInvoiceResult&fpdm={}&fphm={}&kprq={}&kjje={}&jym={}' \
                    .format(InvoiceCode, InvoiceNum, InvoiceDate, TotalAmount, CheckCode)
                UA = {"user-agent": "Mozilla/5.0 (Windows NT 6.1; rv:2.0.1) Gecko/2010101 Firefox/4.0.1"}
                response = requests.get(url, headers=UA).text
                result = json.loads(response)
                if result['errorCode'] == '0' and result['data'] != 'null':
                    data["Authenticity"] = "True"
                    return json.dumps(data, ensure_ascii=False)
                else:
                    data["Authenticity"] = "False"
                    return json.dumps(data, ensure_ascii=False)
            else:
                data["Authenticity"] = "MissingInfo"
                return json.dumps(data)
        else:
            return "wrong params: type '{}'".format(dicts['type'])
    else:
        return access_token
