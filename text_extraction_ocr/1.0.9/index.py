import json
import os
import base64
import time
import requests


def get_token(api_key='', secret_ket=''):
    # https://ai.baidu.com/docs#/Auth/75d80ed1
    # https://console.bce.baidu.com/ai/?_=1566193744774&fromai=1#/ai/ocr/app/detail~appId=289184
    # api_key = 'R5LAXesAqaNYunPWLb5UHM3h'
    # secret_ket = 'RdLi2pn2usBfGVTSSHedQUlrkO1FUnIV'
    token_url = f'https://aip.baidubce.com/oauth/2.0/token?' \
        f'grant_type=client_credentials&client_id={api_key}&client_secret={secret_ket}&'
    res = requests.post(token_url)
    if 'access_token' in res.json().keys():
        access_token = res.json()['access_token']
        return access_token
    else:
        return 'error'


def encode_image(path):
    try:
        size = os.path.getsize(path)
        if size > 4194304:
            return "image size too big"
        f = open(path, 'rb')
        img = base64.b64encode(f.read())
        return img
    except Exception as error:
        return error


def ocr_set_params(params):
    access_token = get_token(params['api_key'], params['secret_ket'])
    if 'error' not in access_token:
        post_data = {'image': encode_image(params['path'])}
        if params['type'] == 'idcard':
            print('身份证')
            url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/idcard?access_token=' + access_token
            res = requests.post(url=url, data=post_data)
            return json.dumps(res.text, ensure_ascii=False)

        else:
            print('增值税发票')
            url = 'https://aip.baidubce.com/rest/2.0/ocr/v1/vat_invoice?access_token=' + access_token
            post_data = {'image': encode_image(params['path']), 'type': params['type']}
            res = requests.post(url, data=post_data)
            try:
                return res.json()
            except Exception as ex:
                return '识别出错：%s' % ex
    else:
        return 'access_token错误，请检查api key和app id'
