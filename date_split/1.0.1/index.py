import datetime


def today(params):
    if '-' in params['date_format']:
        fmt = '%Y-%m-%d'
    elif '/' in params['date_format']:
        fmt = '%Y/%m/%d'
    elif params['date_format'].__len__() == 8:
        fmt = '%Y%m%d'
    return datetime.datetime.now().strftime(fmt)


def yesterday(params):
    if '-' in params['date_format']:
        fmt = '%Y-%m-%d'
    elif '/' in params['date_format']:
        fmt = '%Y/%m/%d'
    elif params['date_format'].__len__() == 8:
        fmt = '%Y%m%d'
    start_date = datetime.datetime.today()
    delta = datetime.timedelta(days=-1)
    temp_date = start_date + delta
    return temp_date.strftime(fmt)


def date_split(params):
    start = params['start_date']
    end = params['end_date']
    date_range = int(params['date_range'])

    if '-' in start:
        fmt = '%Y-%m-%d'
    elif '/' in start:
        fmt = '%Y/%m/%d'
    elif start.__len__() == 8:
        fmt = '%Y%m%d'
    date_set = []
    start_date = datetime.datetime.strptime(start, fmt)
    end_date = datetime.datetime.strptime(end, fmt)
    delta = datetime.timedelta(days=date_range)
    delta_1 = datetime.timedelta(days=1)
    while start_date <= end_date:
        temp_date = start_date + delta
        if temp_date >= end_date:
            temp_date = end_date
        date_set.append([start_date.strftime(fmt), temp_date.strftime(fmt)])
        start_date = temp_date + delta_1
    return date_set
