"""
by suyj
"""
import os
from openpyxl import Workbook, load_workbook
import datetime


def write_excel(params):
    if 'data' not in params.keys() or params['data'] is None:
        raise Exception("缺少参数：行数据")
    elif not isinstance(params['data'], list):
        raise Exception("行数据类型必须为数组")
    elif 'excel_path' not in params.keys() or params['excel_path'] is None or params['excel_path'] == '':
        raise Exception("缺少参数：excel保存位置")
    elif 'file_name' not in params.keys() or params['file_name'] is None or params['file_name'] == '':
        raise Exception("缺少参数：保存文件名")
    elif 'write_mode' not in params.keys() or params['write_mode'] is None or params['write_mode'] == '':
        raise Exception("缺少参数：写入模式")
    else:
        file_name = params['file_name']
        file_path = os.path.join(params['excel_path'], file_name)
        if params['write_mode'] == 'append':
            if not os.path.exists(file_path):
                wb = Workbook()
            else:
                wb = load_workbook(file_path)
        elif params['write_mode'] == 'cover':
            wb = Workbook()
        else:
            raise Exception('尚未支持该写入模式')
        data_list = params['data']
        ws = wb.active
        for data in data_list:
            ws.append(data)
        wb.save(file_path)
        return file_path
