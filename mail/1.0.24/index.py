import os
import bs4
import smtplib
import email
from email import policy
from email.mime.text import MIMEText
from email.header import Header, decode_header
from email.mime.application import MIMEApplication
from email.mime.multipart import MIMEMultipart
import poplib
from email.parser import Parser, BytesParser  # 解析模块
from email.utils import parseaddr, formataddr, parsedate, parsedate_tz, parsedate_to_datetime  # 用于格式化邮件信息
import time
import chardet
import json
import re
from imbox import Imbox
import datetime
import urllib.request
import urllib.parse
import imaplib
import arrow
import glob
import shutil
import base64
import quopri
import moment
import hashlib
import hmac
import traceback

poplib._MAXLINE=20480


codes = ["gbk", "utf8", "ascii", "unicode_escape"]
default_code = ""

attachments_template = """
<div id="attachment" style="padding:2px;background-color: #cccccc;">
    <div style="padding:5px 10px;" class="txt_left"><b style="font-size:14px;">
            <svg t="1596180706255" style="width: 16px;height: 16px;" class="icon" viewBox="0 0 1024 1024" version="1.1" xmlns="http://www.w3.org/2000/svg"
                p-id="1162">
                <path
                    d="M672.1 64.2c25.7 0 51.4 6.4 70.7 12.9C820 109.2 865 180 865 263.6c0 51.4-19.3 102.9-57.9 141.5L447 771.6c-12.9 12.9-25.7 19.3-38.6 25.7-12.9 6.4-25.7 6.4-45 6.4-12.9 0-32.2 0-45-6.4-12.9-6.4-25.7-12.9-32.2-25.7-12.9-12.9-19.3-25.7-25.7-38.6-6.4-12.9-6.4-32.1-6.4-45 0-32.2 12.9-64.3 32.2-83.6l328-340.8c6.4-6.4 19.3-12.9 25.7-12.9 12.9 0 25.7 6.4 32.2 12.9 6.4 6.4 12.9 19.3 12.9 25.7-6.4 12.9-6.4 19.3-12.9 32.2L337.7 655.8c-6.4 6.4-12.9 19.3-12.9 32.2 0 19.3 19.3 38.6 38.6 38.6 12.9 0 19.3-6.4 25.7-12.9l360.1-366.5c25.7-25.7 32.1-51.4 32.1-83.6 0-32.2-12.9-57.9-32.1-83.6-19.3-25.7-45-38.6-77.2-38.6s-64.3 12.9-83.6 38.6l-360 366.5c-19.3 19.3-32.2 38.6-45 64.3-19.3 45-19.3 102.9 0 147.9 12.9 25.7 25.7 45 45 64.3 38.6 38.6 83.6 57.9 135 57.9 51.4 0 102.9-19.3 135-57.9L865 462.9c6.4-12.9 12.9-12.9 25.7-12.9 12.9 0 19.3 6.4 25.7 12.9 6.4 6.4 12.9 12.9 12.9 25.7s-6.4 19.3-12.9 25.7L556.3 880.9c-25.7 25.7-57.9 45-90 64.3-32.2 12.9-64.3 19.3-102.9 19.3-38.6 0-70.7-6.4-102.9-19.3s-64.3-32.2-90-64.3c-25.7-25.7-45-57.9-57.9-90-12.8-32.2-19.2-70.8-19.2-102.9 0-38.6 6.4-70.7 19.3-109.3 12.9-32.2 32.2-64.3 57.9-90l360.1-366.5c38.5-38.7 89.9-58 141.4-58z m0 0"
                    p-id="1163"></path>
            </svg> 附件</b></div>
    <div style="padding:0 8px 6px 20px;background:#fff;line-height:140%;overflow: hidden;zoom: 1;">
        <div class="att_bt">
            <div class="name_big">
                {{attachment_template}}
            </div>
        </div>
    </div>
</div>
"""


def get_email_message(file_path=None, eml_content=None):
    global default_code
    email_message = None
    if file_path:
        for encoding in codes:
            try:
                infile = open(file_path, "r", encoding=encoding)
                email_message = email.message_from_file(infile)
                default_code = encoding
                break
            except UnicodeDecodeError as e:
                print(e)
                email_message = None
    else:
        email_message = email.message_from_bytes(eml_content)

    return email_message


def decode_email_text(text):
    if text is not None and text != "":
        try:

            h = email.header.Header(text)
            dh = email.header.decode_header(h)

            if dh[0][1]:
                s = str(dh[0][0], dh[0][1])
                value, ch = email.header.decode_header(s)[0]
                if ch:
                    ch = ("gbk" if ch.lower() == 'gb2312' else ch)
                    text = value.decode(ch)

            text = text.replace("\n", "").replace(
                "\r", "").replace("?= =?", "?==?")
            base64_str_list = re.findall(r'\=\?(.*?)\?(B|b)\?(.*?)\?\=', text)

            if len(base64_str_list) > 0:
                for base64_str in base64_str_list:
                    text_code = (
                        "gbk" if base64_str[0] == "gb2312" else base64_str[0])
                    decode_base64_str = base64.b64decode(
                        base64_str[-1]).decode(text_code)
                    text = text.replace("=?%s?%s?%s?=" % (
                        base64_str[0], base64_str[1], base64_str[-1]), decode_base64_str)

            quopri_str_list = re.findall(r'\=\?(.*?)\?(Q|q)\?(.*?)\?\=', text)
            if len(quopri_str_list) > 0:
                for quopri_str in quopri_str_list:
                    text_code = (
                        "gbk" if quopri_str[0] == "gb2312" else quopri_str[0])
                    decode_quopri_str = quopri.decodestring(
                        quopri_str[1]).decode(text_code)
                    text = text.replace("=?%s?%s?%s?=" % (
                        quopri_str[0], quopri_str[1], quopri_str[-1]), decode_quopri_str)

            return text
        except Exception:
            print(traceback.format_exc())
            print(text)
            raise Exception("转码失败")
    else:
        return text


class EmailData():

    def __init__(self):
        pass

    def set_value(self, key, value):
        setattr(self, key, value)


def resolve_eml_file(params):
    if params["data_type"] == "file" and ("eml_path" not in params.keys() or not params["eml_path"]):
        raise Exception("缺少参数：EML文件路径")
    elif params["data_type"] == "stream" and ("stream_data" not in params.keys() or not params["stream_data"]):
        raise Exception("缺少参数：数据对象")
    elif params["data_type"] == "stream" and ("output_dir" not in params.keys() or not params["output_dir"]):
        raise Exception("缺少参数：输出目录")
    else:
        standard_format = '%a, %d %b %Y %H:%M:%S +%f'
        save_format = "%Y-%m-%d %H:%M:%S %f"

        params['output_dir'] = (os.path.dirname(params['eml_path']) if params['data_type'] == "file" else params['output_dir'])

        email_data = EmailData()
        if params["data_type"] == "file":
            msg = get_email_message(file_path=params['eml_path'])
        else:
            msg = get_email_message(eml_content=params['stream_data'])

        if msg is not None:
            keys = msg.keys()
            if len(keys) == 0:
                raise Exception("eml文件内容有误")

            for key in msg.keys():
                if key == "Received":
                    email_data.set_value(key=key.lower(), value=msg.get_all(key))
                else:
                    email_data.set_value(key=key.lower(), value=decode_email_text(text=msg.get(key)))

            hash_secret = "rpa-email".encode("utf8")
            uniq_id_str = (getattr(email_data, "message-id") + " " + email_data.date).encode('utf8')
            email_data.set_value(key='uniq_id', value=hmac.new(hash_secret, uniq_id_str, hashlib.sha256).hexdigest())

            email_data.set_value(key="content", value="")
            email_data.set_value(key="attachments", value=[])

            eml_filename = email_data.subject.replace("\t", "")
            eml_filename = (os.path.basename(params['eml_path']) if params['data_type'] == "file" else re.sub(r'[\/:*?"<>|]', "_", email_data.subject))
            eml_filename = eml_filename.replace("\t", "")

            # date_label = datetime.datetime.now().strftime("%Y-%m-%d")
            # relative_dir = "%s\\%s_%s" % (
            #     date_label, eml_filename, email_data["uniq_id"])
            # email_dir = "%s\\%s" % (params['output_dir'], relative_dir)
            email_dir = params["output_dir"]
            if not os.path.exists(email_dir):
                os.makedirs(email_dir, exist_ok=True)

            email_content_type = None

            # 邮件内容转码及附件转存 BEGIN
            for part in msg.walk():

                filename = part.get_filename()

                if filename is not None:
                    filename = decode_email_text(filename)
                    filename = filename.replace("\t", "")
                    filename = re.sub(r'[\/:*?"<>|]', "_", filename)

                    attach_path = "%s\\%s" % (email_dir, filename)

                    if not os.path.exists(attach_path):
                        attach_file = open(attach_path, "wb")
                        attach_file.write(part.get_payload(decode=True))
                        attach_file.close()

                    email_data.attachments.append({
                        "content_type": part.get_content_type(),
                        "filename": filename,
                        "path": "%s\\%s" % (email_dir, filename)
                    })
                else:

                    content = part.get_payload(decode=True)
                    email_content_type = part.get_content_type()

                    if content:

                        if not isinstance(content, str):
                            for encoding in codes:
                                try:
                                    email_data.content = content.decode(encoding)
                                    break
                                except UnicodeDecodeError:
                                    pass
                        else:
                            email_data.content = decode_email_text(content)

            # 邮件内容转码及附件转存 END

            # 将邮件内容生成html BEGIN
            html_file_path = "%s\\content.html" % email_dir
            with open(html_file_path, "w", encoding="utf8") as html_file:
                html_content = ""
                if email_content_type == "text/html":
                    html_content = email_data.content
                else:
                    html_content = "<p>%s</p>" % email_data.content.replace(
                        "\n", "<br>").replace("\r", "<br>").replace("\t", "&emsp;")

                if len(email_data.attachments) > 0:
                    global attachments_template
                    attachment_items = ""
                    for attach in email_data.attachments:
                        attachment_items = attachment_items + \
                            '<a href="%s" download="%s"><span>%s</span></a>&emsp;' % (
                                "#", attach["filename"], attach["filename"])
                    html_content = html_content + \
                        attachments_template.replace(
                            "{{attachment_template}}", attachment_items)

                html_file.write(html_content)
            # 将邮件内容生成html END

            if params['data_type'] == "stream":
                params['eml_path'] = "%s\\%s.eml" % (email_dir, eml_filename)
                with open(params['eml_path'], "wb") as new_eml:
                    new_eml.write(params["stream_data"])

            email_data.set_value(key="eml_path", value=params['eml_path'])

        else:
            raise Exception("读取eml文件出错")

        return email_data


def synchronize_inbox(params):
    """
    同步收件箱邮件至本地目录
    """
    if 'email_object' not in params.keys() or not params['email_object']:
        raise Exception("缺少参数：邮箱对象")
    elif 'storage_dir' not in params.keys() or not params['storage_dir']:
        raise Exception("缺少参数：本地存储目录")
    else:
        new_email_list = []
        email_object = params['email_object']
        storage_dir = params['storage_dir']

        email_ids = email_object.list()[1]
        for i, email_id in enumerate(email_ids):
            email_id = email_object.uidl(i + 1).decode().split(" ")[-1]
            email_dir = "%s\\%s" % (storage_dir, email_id)
            if not os.path.exists(email_dir):
                os.makedirs(email_dir)

                try:
                    resp, lines, octets = email_object.retr(i + 1)
                    email_content = b'\r\n'.join(lines)
                    email_data = resolve_eml_file({
                        "data_type": "stream",
                        "stream_data": email_content,
                        "output_dir": email_dir
                    })

                    email_data.uniq_id = email_id
                    new_email_list.append(email_data)
                except Exception:
                    print(traceback.format_exc())
                    shutil.rmtree(email_dir)
            time.sleep(1)
    
    return new_email_list



def send_mail(params):
    try:
        to = str(params['toMail'])
        title = str(params['title'])
        content = str(params['content'])
        port = int(params['port'])
        port_ssl = int(params['port_ssl'])
        ssl = params['ssl']
        # addresser = params['addresser']
        server = ''
        if params['server'] == 'qq':
            server = "smtp.qq.com"
        elif params['server'] == 'gmail':
            server = 'smtp.gmail.com'
        elif params['server'] == '126':
            server = "smtp.126.com"
        elif params['server'] == '163':
            server = 'smtp.163.com'
        elif params['server'] == 'custom':
            server = params['custom_server']

        find_account = re.findall(r'<(.*?)>', str(params['account']))
        if len(find_account) > 0:
            account = find_account[0]
            sender = str(params['account'])
        else:
            account = str(params['account'])
            sender = "%s<%s>" % (account.split('@')[0], account)

        password = str(params['password'])

        if params['content_format'] == "html":
            message = MIMEText(content, 'html', 'utf-8')
        elif params['content_format'] == "plain":
            message = MIMEText(content, 'plain', 'utf-8')
        else:
            message = MIMEText(content, 'plain', 'utf-8')

        m = MIMEMultipart()
        m.attach(message)
        if params['file'] != "":
            if bool(params['reset']) is False:
                mailFile = params['file']
                *_, filename = os.path.split(mailFile)
                sendfile = MIMEApplication(open(mailFile, 'rb').read())
                sendfile.add_header('Content-Disposition',
                                    'attachment', filename=filename)
                m.attach(sendfile)

        if params['files'] != '':
            if bool(params['resets']) is False:
                filespath = params['files']
                if os.path.exists(filespath):
                    for file in os.listdir(filespath):
                        if os.path.isfile(filespath + "\\" + file):
                            sendfiles = MIMEApplication(
                                open(filespath + "\\" + file, 'rb').read())
                            sendfiles.add_header(
                                'Content-Disposition', 'attachment', filename=file)
                            m.attach(sendfiles)

        m['To'] = ""
        receiver_list = to.split(";")
        receivers = []
        for receiver in receiver_list:
            find_rec = re.findall(r"<(.*?)>", receiver)
            if len(find_rec) > 0:
                receivers.append(find_rec[0])
                temp = _format_addr(receiver)
            else:
                receivers.append(receiver)
                temp = _format_addr(receiver.split('@')[0] + '<%s>' % receiver)

            if m['To'] != "":
                m['To'] = m['To'] + ";" + temp
            else:
                m['To'] = temp

        # _format_addr(account.split('@')[0] + '<%s>' % account)
        m['From'] = sender
        # m['To'] = _format_addr(to.split('@')[0] + '<%s>' % to)
        m['Subject'] = Header(title, 'utf-8').encode()

        smtpObj = None
        if 'no' == ssl:
            smtpObj = smtplib.SMTP()
        elif 'yes' == ssl:
            smtpObj = smtplib.SMTP_SSL(host=server)

        smtpObj.connect(server, port=port if ssl == 'no' else port_ssl)
        smtpObj.login(account, password)
        smtpObj.sendmail(account, receivers, m.as_string())
        smtpObj.quit()
    except Exception as e:
        raise e
    return "邮件发送成功"


def connectMail(params):
    port = int(params['port'])
    port_ssl = int(params['port_ssl'])
    ssl = params['ssl']
    server = ''
    account = params['account']
    password = params['password']
    popObj = None
    try:
        if params['prot'] == 'pop3':
            server = params['server_pop']
            if params['server_pop'] == 'custom':
                server = params['custom_server']
            print(server)
            if ssl == 'no':
                popObj = poplib.POP3(
                    server, port=port if ssl == 'no' else port_ssl)
            elif ssl == 'yes':
                popObj = poplib.POP3_SSL(
                    server, port=port if ssl == 'no' else port_ssl)

            popObj.user(account)
            popObj.pass_(password)

            print("连接邮箱成功")
            print(popObj.getwelcome())
        elif params['prot'] == 'IMAP':
            server = params['server_imap']
            if params['server_imap'] == 'custom':
                server = params['custom_server']
            print(server)
            ssl = False if params['ssl'] == 'no' else True
            popObj = Imbox(server,
                           username=account,
                           password=password,
                           ssl=ssl)
            # with Imbox(server, account, password, ssl_context=None, starttls=False) as imbox:
            #     print(imbox)
            #     for uid, mail in imbox.messages():
            #         # print('====')
            #         print("C", uid)
            print("连接邮箱成功")
        return popObj

    except Exception as e:
        raise e


def getTitle(params):
    num = int(params['num'])
    popObj = params['obj']
    title = None
    try:
        if isinstance(popObj, Imbox):
            mails = popObj.messages()
            # print(mails)
            # count = len(mails)
            # print(count)
            # if num > count:
            #     return "邮件序号错误"
            # for uid, mail in mails:
            #     if mail:
            #         print(uid, mail.subject, mail.sent_from)
            # print(uid, mail)
            # title = mails[count - num][1].subject
            # if num == 0:
            #     num = 1
            title = mails[-num][1].subject

        else:
            mails = popObj.list()[1]
            count = len(mails)
            print("邮件数量：", len(mails))
            if num > count:
                return "邮件序号错误"

            # for m in range(len(mails)):
            resp, lines, octets = popObj.retr(count - num + 1)
            # resp, lines, octets = popObj.retr(m+1)

            msg_content = b'\r\n'.join(lines).decode()

            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象

            title = msg.get('Subject', '')
            if title:
                title = decode_str(title)
        # print(title)

        return title
    except Exception as e:
        raise e


def getContent(params):
    num = int(params['num'])
    popObj = params['obj']
    isFilter = False if params['filter'] == 'no' else True
    content = ''
    try:
        if isinstance(popObj, Imbox):
            mails = popObj.messages()
            # print(mails)
            count = len(mails)
            print(count)
            if num > count:
                return "邮件序号错误"
            # for uid, mail in mails:
            # print(uid, mail.subject, mail.sent_from)
            # print(uid, mail)
            body = mails[-num][1].body
            # print(body)
            # if len(body['plain']) > 0:
            #     content = ''.join(body['plain'])
            # elif len(body['html']) > 0:
            #     content = "".join(body['html'])
            content = {
                'plain': ''.join(body['plain']),
                'html': ''.join(body['html'])
            }
            # if isFilter:
            #     bs = bs4.BeautifulSoup(content, "html.parser")
            #     [b.extract() for b in bs(['script', 'style'])]
            #     content = bs.text.lstrip().rstrip()
        else:
            mails = popObj.list()[1]
            count = len(mails)
            if num > count:
                return "邮件序号错误"
            resp, lines, octets = popObj.retr(count - num + 1)
            msg_content = b'\r\n'.join(lines).decode('utf-8')
            content = ''
            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
            # print(msg)
            for part in msg.walk():
                # print(part)
                content_type = part.get_content_type()
                # print(content_type)
                charset = guess_charset(part)
                # 如果有附件，则直接跳过
                if part.get_filename() is not None:
                    continue
                email_content_type = ''
                if charset:
                    print("guess_charset::", charset)
                    if 'utf-8' in charset:
                        charset = 'utf-8'
                    if charset == 'us-ascii':
                        continue
                    if content_type == 'text/plain':
                        print("text/plain===============")
                        email_content_type = 'text'
                        content = part.get_payload(decode=True).decode(
                            charset, errors='ignore')
                    elif content_type == 'text/html':
                        print("text/html===============")
                        email_content_type = 'html'
                        content = part.get_payload(decode=True).decode(
                            charset, errors='ignore')
                        # print(content)
                        if isFilter:
                            bs = bs4.BeautifulSoup(content, "html.parser")
                            [b.extract() for b in bs(['script', 'style'])]
                            content = bs.text.lstrip().rstrip().encode(
                                charset, errors='ignore')  # .decode()
                            charset = chardet.detect(content)['encoding']
                            print("编码：", chardet.detect(content))
                            if charset == 'ascii':
                                charset = 'unicode_escape'
                            if charset:
                                content = content.decode(charset)
                            else:
                                content = content.decode()
                    # print(content)

                if email_content_type == '':
                    continue
            # if content == '':
            #     return "该邮件内容为空"

        return content
    except Exception as e:
        raise e


def getFrom(params):
    num = int(params['num'])
    popObj = params['obj']
    FROM = ''
    try:
        if isinstance(popObj, Imbox):
            mails = popObj.messages()
            # print(mails)
            count = len(mails)
            print(count)
            if num > count:
                return "邮件序号错误"
            # for uid, mail in mails:
            # print(uid, mail.subject, mail.sent_from)
            # print(uid, mail)
            FROM = mails[-num][1].sent_from[0]['name']
        else:
            mails = popObj.list()[1]
            count = len(mails)
            if num > count:
                return "邮件序号错误"
            resp, lines, octets = popObj.retr(count - num + 1)
            msg_content = b'\r\n'.join(lines).decode('utf-8')

            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象

            FROM = parseaddr(msg.get("From"))[0]

            if FROM:
                FROM = decode_str(FROM)

        return FROM
    except Exception as e:
        raise e


def getEmail(params):
    num = int(params['num'])
    popObj = params['obj']
    email = ''
    try:
        if isinstance(popObj, Imbox):
            mails = popObj.messages()
            # print(mails)
            count = len(mails)
            print(count)
            if num > count:
                return "邮件序号错误"
            # for uid, mail in mails:
            # print(uid, mail.subject, mail.sent_from)
            # print(uid, mail)
            email = mails[-num][1].sent_from[0]['email']
        else:
            mails = popObj.list()[1]
            count = len(mails)
            if num > count:
                return "邮件序号错误"
            resp, lines, octets = popObj.retr(count - num + 1)
            msg_content = b'\r\n'.join(lines).decode('utf-8')
            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
            email = parseaddr(msg.get("from"))[1]

            if email:
                email = decode_str(email)

        return email
    except Exception as e:
        raise e


def getDate(params):
    num = int(params['num'])
    popObj = params['obj']
    date = ''
    try:
        if isinstance(popObj, Imbox):
            mails = popObj.messages()
            # print(mails)
            count = len(mails)
            print(count)
            if num > count:
                return "邮件序号错误"
            # for uid, mail in mails:
            # print(uid, mail.subject, mail.sent_from)
            # print(uid, mail)
            date = mails[-num][1].date
        else:
            mails = popObj.list()[1]
            count = len(mails)
            if num > count:
                return "邮件序号错误"
            resp, lines, octets = popObj.retr(count - num + 1)
            msg_content = b'\r\n'.join(lines).decode('utf-8')
            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
            date = msg.get("Date")
            t = parsedate(date)
            stamp = time.mktime(t)
            timeArray = time.localtime(stamp)
            date = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
        return date
    except Exception as e:
        raise e


def getFile(params):
    num = int(params['num'])
    path = params['path']
    popObj = params['obj']
    file_list = list()
    try:
        if isinstance(popObj, Imbox):
            mails = popObj.messages()
            # print(mails)
            count = len(mails)
            print(count)
            if num > count:
                return "邮件序号错误"
            # for uid, mail in mails:
            # print(uid, mail.subject, mail.sent_from)
            # print(uid, mail)
            mail = mails[-num][1]
            if mail.attachments:
                print("附件数：", len(mail.attachments))
                for attachment in mail.attachments:
                    print(attachment)
                    file_name = attachment['filename']
                    file_name = urllib.request.unquote(str(file_name))
                    print(file_name)
                    print(type(file_name))
                    file_name = file_name.replace('utf-8\'\'', '')
                    print(file_name)
                    file_list.append(os.path.join(path, file_name))
                    if os.path.exists(os.path.join(path, file_name)):
                        os.remove(os.path.join(path, file_name))
                    with open(os.path.join(path, file_name), 'wb') as f:
                        f.write(attachment['content'].getvalue())

        else:
            mails = popObj.list()[1]
            count = len(mails)
            if num > count:
                return "邮件序号错误"
            resp, lines, octets = popObj.retr(count - num + 1)
            msg_content = b'\r\n'.join(lines).decode('utf-8', errors='ignore')
            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
            result = "该邮件没有附件"
            # downfile(msg, path)
            for part in msg.walk():
                file_name = part.get_filename()
                if file_name:
                    fileName = Header(file_name)
                    fileName = decode_header(fileName)  # 对附件名称进行解码
                    filename = fileName[0][0]
                    if fileName[0][1]:
                        filename = decode_str(
                            str(filename, fileName[0][1]))  # 将附件名称可读化
                    data = part.get_payload(decode=True)  # 下载附件
                    if os.path.exists(os.path.join(path, filename)):
                        os.remove(os.path.join(path, filename))
                    # 在指定目录下创建文件，注意二进制文件需要用wb模式打开
                    with open(os.path.join(path, filename), 'wb') as att_file:
                        att_file.write(data)  # 保存附件
                    file_list.append(os.path.join(path, filename))
                    result = "保存附件成功"
            print(result)
        return file_list
    except Exception as e:
        raise e


def decode_str(s):
    value, charset = decode_header(s)[0]
    if charset:
        print("header_charset:::", charset)
        value = value.decode(charset, errors='ignore')  # 如果文本中存在编码信息，则进行相应的解码
    return value


def guess_charset(msg):
    charset = msg.get_charset()  # 直接用get_charset()方法获取编码
    if charset is None:  # 如果获取不到，则在原始文本中寻找
        print("charset is none")
        content_type = msg.get('Content-Type', '').lower()
        pos = content_type.find('charset=')  # 找'charset='这个字符串
        if pos >= 0:  # 如果有，则获取该字符串后面的编码信息
            charset = content_type[pos + 8:].strip()
            print("find charset::", charset)
    return charset


def _format_addr(s):
    name, addr = parseaddr(s)
    # .encode('utf-8') if isinstance(addr, str) else addr))
    return formataddr((Header(name, 'utf-8').encode(), addr))


def downfile(msg, path):
    for part in msg.walk():
        filename = part.get_filename()
        if filename is not None:  # 如果存在附件
            filename = decode_str(filename)  # 获取的文件是乱码名称，通过一开始定义的函数解码
            data = part.get_payload(decode=True)  # 取出文件正文内容
            # 此处可以自己定义文件保存位置
            f = open(os.path.join(path, filename), 'wb')
            f.write(data)
            f.close()


def get_mails_by_dates(params):
    try:
        # popObj = connectMail(params)
        popObj = params['obj']
        isFilter = False if params['filter'] == 'no' else True
        mails = list()
        # mails.clear()
        result = list()
        mails = popObj.list()[1]
        count = len(mails)
        print("邮件数量：", count)
        the_date = ''
        for index in range(1, count + 1):
            resp, lines, octets = popObj.retr(count - index + 1)
            msg_content = b'\r\n'.join(lines).decode('utf-8', errors='ignore')
            msg = Parser().parsestr(msg_content)  # 把邮件内容解析成message对象
            date = msg.get("Date")
            print('date>>>>>>>>>>>>>>>', date)
            if date is None:
                continue
            if '(' in date:
                # date = date.replace('(CST)', '').strip()
                the_date = date[:date.index('(')].strip()
            elif 'GMT' in date and '(' not in date:
                the_date = date.replace('GMT', '').strip()
            else:
                the_date = date
            if "+" in the_date:
                the_date = the_date[:the_date.index('+')].strip()
            print('the-day>>>>>>>>>>>', the_date)
            # if params['server'] == '163' or params['server'] == '126':
            #     if ',' in the_date:
            #         the_date = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%a, %d %b %Y %H:%M:%S"))
            #     else:
            #         the_date = time.strftime('%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%d %b %Y %H:%M:%S"))
            # else:
            if ',' in the_date:
                the_date = time.strftime(
                    '%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%a, %d %b %Y %H:%M:%S"))
            else:
                the_date = time.strftime(
                    '%Y-%m-%d %H:%M:%S', time.strptime(the_date, "%d %b %Y %H:%M:%S"))

            if params['date_end'] >= the_date >= params['date_start']:
                mail_info = {'index': index}
                title = msg.get('Subject', '')
                if title:
                    title = decode_str(title)
                mail_info['title'] = title
                content = ''
                for part in msg.walk():
                    content_type = part.get_content_type()
                    # print(content_type)
                    charset = guess_charset(part)
                    # 如果有附件，则直接跳过
                    if part.get_filename() is not None:
                        continue
                    email_content_type = ''
                    if charset:
                        print("guess_charset::", charset)
                        if 'utf-8' in charset:
                            charset = 'utf-8'
                        if charset == 'us-ascii':
                            continue
                        if content_type == 'text/plain':
                            email_content_type = 'text'
                            content = part.get_payload(decode=True).decode(
                                charset, errors='ignore')
                        elif content_type == 'text/html':
                            print("text/html===============")
                            email_content_type = 'html'
                            content = part.get_payload(decode=True).decode(
                                charset, errors='ignore')
                            # print(content)
                            if isFilter:
                                bs = bs4.BeautifulSoup(content, "html.parser")
                                [b.extract() for b in bs(['script', 'style'])]
                                content = bs.get_text().lstrip().rstrip().encode(
                                    charset, errors='ignore')  # .decode()
                                charset = chardet.detect(content)['encoding']
                                print("编码：", chardet.detect(content))
                                if charset == 'ascii':
                                    charset = 'unicode_escape'
                                if charset:
                                    print('charset:>>>>>>>>>>>>>', charset)
                                    content = content.decode(charset)
                                else:
                                    content = content.decode(
                                        guess_charset(part))
                            # temp = '"%s"' % content
                            # content = json.loads(temp, strict=False).replace(u"\u3000", ' ')
                    if email_content_type == '':
                        continue
                mail_info['content'] = content
                FROM = parseaddr(msg.get("From"))[0]
                if FROM:
                    FROM = decode_str(FROM)
                mail_info['from'] = FROM
                email = parseaddr(msg.get("from"))[1]
                if email:
                    email = decode_str(email)
                mail_info['email'] = email
                mail_info['date'] = the_date

                mails.append(mail_info)

            if the_date < params['date_start']:
                break
        for mail in mails:
            if isinstance(mail, dict):
                result.append(mail)

        print('len>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>>', len(result))
        return result
    except Exception as e:
        raise e


def getEml(params):
    num = int(params['num'])
    popObj = params['obj']
    file_path = os.path.join(params['path'], params['name'])
    try:
        if isinstance(popObj, Imbox):
            # mails = popObj.messages()
            # # print(mails)
            # count = len(mails)
            # print(count)
            # # if num > count:
            # #     return "邮件序号错误"
            # body = mails[-num][1].body
            # content = {
            #     'plain': ''.join(body['plain']),
            #     'html': ''.join(body['html'])
            # }
            # text = ''
            # if content['html']:
            #     text = content['html']
            # else:
            #     text = content['plain']
            # if os.path.exists(file_path):
            #     os.remove(file_path)
            # with open(file_path, 'w') as f:
            #     f.write(text)
            return "暂不支持IMAP协议"
        else:
            mails = popObj.list()[1]
            count = len(mails)
            if num > count:
                return "邮件序号错误"
            resp, lines, octets = popObj.retr(count - num + 1)
            msg_content = b'\r\n'.join(lines)  # .decode('utf-8')
            if os.path.exists(file_path):
                os.remove(file_path)
            with open(file_path, 'wb') as f:
                f.write(msg_content)

    except Exception as e:
        raise e


def Imap():
    conn = imaplib.IMAP4_SSL("imap.qq.com")
    conn.login("1336649658@qq.com", "tdzgopujsyazjfce")
    # 选定一个邮件文件夹
    conn.select("INBOX")  # 获取收件箱
    mails = conn.search(None, 'ALL')[1]
    date_mails = conn.search(
        None, '(since "29-Jul-2020" before "30-Jul-2020")')[1]
    date_mails_list = date_mails[0].split()
    mails_list = mails[0].split()
    mails_list = list(reversed(mails_list))
    mail_nums = len(mails_list)
    # print(mail_nums)
    print(len(date_mails_list))
    for i in range(mail_nums):
        try:
            if i == 50:
                break
            print("mail: {}/{}".format(i + 1, mail_nums))
            data = conn.fetch(mails_list[i], '(RFC822)')[1]
            email_body = data[0][1]
            coding = chardet.detect(email_body)['encoding']
            mail = email.message_from_bytes(email_body)
            # print(mail)
            mail_encode = decode_header(mail.get("Subject"))[0][1]
            mail_title = decode_data(decode_header(mail.get("Subject"))[
                                     0][0], [coding, mail_encode])
            sender_mail = parseaddr(mail.get('From'))[1]
            sender = decode_data(decode_header(mail.get('From'))[0][0],
                                 [coding, mail_encode, decode_header(mail.get('From'))[0][1]])
            date = mail.get('Date')
            t = parsedate(date)
            stamp = time.mktime(t)
            timeArray = time.localtime(stamp)
            new_date = time.strftime("%Y-%m-%d %H:%M:%S", timeArray)
            print('title: ', mail_title)
            print('from: ', sender)
            print('send_email: ', sender_mail)
            print('parsedate: ', new_date)
            print('========================================')
        except Exception as e:
            print(e)


def decode_data(bytes, added_encode=None):
    """
    字节解码
    :param bytes:
    :return:
    """

    def _decode(bytes, encoding):
        try:
            return str(bytes, encoding=encoding)
        except Exception as e:
            return None

    encodes = ['UTF-8', 'GBK', 'GB2312', 'ascii', 'gb2312', 'gb18030']
    if added_encode:
        encodes = added_encode + encodes
    for encoding in encodes:
        str_data = _decode(bytes, encoding)
        if str_data is not None:
            return str_data
    return ''


def parse_mail_time(mail_datetime):
    """
    邮件时间解析
    :param bytes:
    :return:
    """
    # print(mail_datetime)
    GMT_FORMAT = "%a, %d %b %Y %H:%M:%S"
    GMT_FORMAT2 = "%d %b %Y %H:%M:%S"
    index = mail_datetime.find(' +0')
    if index > 0:
        mail_datetime = mail_datetime[:index]  # 去掉+0800
    index = mail_datetime.find(' -0')
    if index > 0:
        mail_datetime = mail_datetime[:index]
    print(mail_datetime)
    formats = [GMT_FORMAT, GMT_FORMAT2]
    for ft in formats:
        try:
            # mail_datetime = datetime.strptime(mail_datetime, ft)
            mail_datetime = time.strftime(
                '%Y-%m-%d %H:%M:%S', time.strptime(mail_datetime, ft))
            return mail_datetime
        except:
            pass

    raise Exception("邮件时间格式解析错误")


if __name__ == '__main__':
    pass
