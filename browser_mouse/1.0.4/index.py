import sys
import json
import base64
import time
import uiautomation as uiauto
from browser import ResumeBrowser, generate_xpath
from selenium.webdriver.common.action_chains import ActionChains


def mouse_click(params):
    try:
        element_type = params['element_type']
        mouse_key = params['mouse_key']
        click_type = params['click_type']
        target_element = params['target_element']

        element = None
        if params['element_type'] == 'Xpath':
            if 'browser_info' in params.keys() and params['browser_info'] is not None:
                driver = params['browser_info']
                element = driver.find_element_by_xpath(params['element_xpath'])
                if element is not None:
                    if mouse_key == 'left':
                        if click_type == 'simple':
                            element.click()
                        elif click_type == 'double':
                            ActionChains(driver).double_click(element).perform()
                        else:
                            raise Exception(message="尚未支持该点击类型")
                    elif mouse_key == 'right':
                        if click_type == 'simple':
                            ActionChains(driver).context_click(element).perform()
                        elif click_type == 'double':
                            ActionChains(driver).context_click(element).perform()
                            time.sleep(.1)
                            ActionChains(driver).context_click(element).perform()
                        else:
                            raise Exception(message="尚未支持该点击类型")
                    else:
                        raise Exception(message="尚未支持该鼠标键的点击操作")
            else:
                raise Exception("缺少参数：浏览器对象")
        elif element_type == 'Browser' and 'html' in target_element.keys():
            if 'browser_info' in params.keys() and params['browser_info'] is not None:
                driver = params['browser_info']
                # driver = ResumeBrowser(
                #     browser_info['executor_url'], browser_info['session_id'])
                html = target_element['html']
                element = None
                print("html的元素信息：", html)
                xpath = generate_xpath(html=html)
                print(xpath)
                element = driver.find_element_by_xpath(xpath)
                if element is not None:
                    if mouse_key == 'left':
                        if click_type == 'simple':
                            element.click()
                        elif click_type == 'double':
                            ActionChains(driver).double_click(element).perform()
                        else:
                            raise Exception(message="尚未支持该点击类型")
                    elif mouse_key == 'right':
                        if click_type == 'simple':
                            ActionChains(driver).context_click(element).perform()
                        elif click_type == 'double':
                            ActionChains(driver).context_click(element).perform()
                            time.sleep(.1)
                            ActionChains(driver).context_click(element).perform()
                        else:
                            raise Exception(message="尚未支持该点击类型")
                    else:
                        raise Exception(message="尚未支持该鼠标键的点击操作")
            else:
                raise Exception("缺少参数：浏览器对象")
        elif element_type == 'Native' and 'wnd' in target_element.keys():
            wnd = target_element['wnd']
            if "control_type_name" not in wnd.keys() or wnd["control_type_name"] is None or wnd["control_type_name"] == "":
                raise Exception(message="客户端界面元素没有control_type_name")
            elif "name" not in wnd.keys() or wnd["name"] is None or wnd["name"] == "":
                raise Exception(message="客户端界面元素没有name属性")
            elif hasattr(uiauto, wnd["control_type_name"]) is False:
                raise Exception(message="uiautomation不支持目标元素类型")
            else:
                control = getattr(uiauto, wnd["control_type_name"])(
                    Name=wnd['name'])
                if mouse_key == 'left':
                    if click_type == 'simple':
                        if hasattr(control, "Click"):
                            control.Click()
                        else:
                            raise Exception(message="当前界面元素不支持键盘输入")
                    elif click_type == 'double':
                        if hasattr(control, "DoubleClick"):
                            control.DoubleClick()
                        else:
                            raise Exception(message="当前界面元素不支持键盘输入")
                    else:
                        raise Exception(message="尚未支持该点击类型")
                elif mouse_key == 'right':
                    if click_type == 'simple':
                        if hasattr(control, "RightClick"):
                            control.RightClick()
                        else:
                            raise Exception(message="当前界面元素不支持键盘输入")
                    elif click_type == 'double':
                        if hasattr(control, "RightClick"):
                            control.RightClick()
                            control.RightClick()
                        else:
                            raise Exception(message="当前界面元素不支持键盘输入")
                    else:
                        raise Exception(message="尚未支持该点击类型")
                else:
                    raise Exception(message="尚未支持该鼠标键的点击操作")
        else:
            raise Exception(message="尚未支持当前元素的操作")

        return None
    except Exception as e:
        print(e)
        raise e

