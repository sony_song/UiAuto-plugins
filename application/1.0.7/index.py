import win32con
import win32process
import psutil
from psutil import NoSuchProcess
import win32api
import win32event
import pywintypes
from win32com.shell.shell import ShellExecuteEx
from win32com.shell import shellcon


def openApp(params):
    try:
        # from win32com.shell.shell import ShellExecuteEx
        # from win32com.shell import shellcon
        path = params['path']
        style = params["style"]
        wait = params["wait"]
        # flags = 0
        # showType = 0
        if style == 'default':
            showType = win32con.SW_NORMAL
            showType = win32con.SW_SHOWNORMAL
        if style == 'max':
            showType = win32con.SW_MAXIMIZE
            showType = win32con.SW_SHOWMAXIMIZED
        if style == 'min':
            showType = win32con.SW_MINIMIZE
            showType = win32con.SW_SHOWMINIMIZED
        if style == 'hide':
            showType = win32con.SW_HIDE
        # if showType == win32con.SW_HIDE:
        #     flags = flags | win32process.CREATE_NO_WINDOW
        # info = win32process.GetStartupInfo()
        # flags = flags | win32process.CREATE_NEW_CONSOLE
        # info.dwFlags = info.dwFlags | win32process.STARTF_USESHOWWINDOW | win32process.STARTF_USESTDHANDLES
        # info.wShowWindow = info.wShowWindow | showType
        # hProcess, hThread, processId, _ = win32process.CreateProcess(
        #     None, path, None, None, False, flags, None, os.path.expanduser('~'), info)
        process_info = ShellExecuteEx(fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                                      hwnd=None, lpVerb='runas', lpFile=path, lpDirectory='', nShow=showType)
        if 'hProcess' in process_info:
            hProcess = process_info['hProcess']
        if 'hInstApp' in process_info:
            hInstApp = process_info['hInstApp']
        Success = False
        if hInstApp > 32:
            Success = True
        if Success:
            if not hProcess:
                return 0
        else:
            return 0
        processId = win32process.GetProcessId(hProcess)
        if wait == 'no':
            # win32api.CloseHandle(hThread)
            win32api.CloseHandle(hProcess)
            return processId
        elif wait == "ready":
            processId = win32process.GetProcessId(hProcess)
            win32event.WaitForInputIdle(hProcess, win32event.INFINITE)
            win32api.CloseHandle(hProcess)
            # win32api.CloseHandle(hThread)
            return processId
        elif wait == "exec":
            win32event.WaitForSingleObject(hProcess, win32event.INFINITE)
            ecode = win32process.GetExitCodeProcess(hProcess)
            win32api.CloseHandle(hProcess)
            # win32api.CloseHandle(hThread)
            return ecode
    except pywintypes.error:
        return processId
    except Exception as e:
        raise e


def openFile(params):
    try:
        # from win32com.shell.shell import ShellExecuteEx
        # from win32com.shell import shellcon
        path = None
        otype = params["type"]
        if otype == "site":
            if params['site'] != '':
                if "http://" in params['site'] or "https://" in params['site']:
                    path = params["site"]
                else:
                    path = "http://" + params["site"]
        if otype == "file":
            if params["path"] != "":
                path = params["path"]
        style = params["style"]
        wait = params["wait"]
        hProcess = None
        hInstApp = None
        if style == 'default':
            style = win32con.SW_NORMAL
            style = win32con.SW_SHOWNORMAL
        elif style == 'max':
            style = win32con.SW_MAXIMIZE
            style = win32con.SW_SHOWMAXIMIZED
        elif style == 'min':
            style = win32con.SW_MINIMIZE
            style = win32con.SW_SHOWMINIMIZED
        elif style == 'hide':
            style = win32con.SW_HIDE
        process_info = ShellExecuteEx(fMask=shellcon.SEE_MASK_NOCLOSEPROCESS,
                                      hwnd=None, lpVerb='open', lpFile=path, lpDirectory='', nShow=style)
        if 'hProcess' in process_info:
            hProcess = process_info['hProcess']
        if 'hInstApp' in process_info:
            hInstApp = process_info['hInstApp']
        Success = False
        if hInstApp > 32:
            Success = True
        if Success:
            if not hProcess:
                return 0
        else:
            return 0
        if wait == 'no':
            pid = win32process.GetProcessId(hProcess)
            win32api.CloseHandle(hProcess)
            return pid
        elif wait == 'ready':
            win32event.WaitForInputIdle(hProcess, win32event.INFINITE)
            pid = win32process.GetProcessId(hProcess)
            win32api.CloseHandle(hProcess)
            return pid
        elif wait == "exec":
            win32event.WaitForSingleObject(hProcess, win32event.INFINITE)
            ecode = win32process.GetExitCodeProcess(hProcess)
            win32api.CloseHandle(hProcess)
            return ecode
    except pywintypes.error:
        return 0
    except Exception as e:
        raise e


def closeApp(params):
    try:
        if params['ptype'] == 'pid':
            pro = psutil.Process(int(params['pid']))
            pro.kill()
        elif params['ptype'] == 'pname':
            pids = psutil.pids()
            for pid in pids:
                pro = psutil.Process(pid)
                if params['pname'] == pro.name():
                    pro.kill()
    except Exception as e:
        raise e


def getAppStatus(params):
    try:
        if params['ptype'] == 'pid':
            pro = psutil.Process(int(params['pid']))
            if pro.status() == 'running':
                return True
        elif params['ptype'] == 'pname':
            pids = psutil.pids()
            pList = list()
            for pid in pids:
                pro = psutil.Process(pid)
                pList.append(pro.name())
            if params['pname'] in pList:
                return True
        return False
    except NoSuchProcess:
        return False
    except Exception as e:
        raise e
